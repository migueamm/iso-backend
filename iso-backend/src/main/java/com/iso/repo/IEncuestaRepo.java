package com.iso.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iso.model.Encuesta;

public interface IEncuestaRepo extends JpaRepository<Encuesta, Integer> {

}
