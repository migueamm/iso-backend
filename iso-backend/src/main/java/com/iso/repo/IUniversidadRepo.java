package com.iso.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iso.model.Universidad;

public interface IUniversidadRepo extends JpaRepository<Universidad, Integer> {

}
