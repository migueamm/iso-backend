package com.iso.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.iso.model.Pregunta;

public interface IPreguntaRepo extends JpaRepository<Pregunta, Integer> {

	@Query(value = "SELECT pr.pre_numero, pr.pre_descripcion, ors.rsp_valor "
			+ "from preguntas pr JOIN opciones_respuestas ors ON pr.pre_id = ors.pre_id "
			+ "JOIN encuestas_respuestas er ON ors.rsp_id = er.rsp_id WHERE pr.pre_numero =:numeroPregunta", nativeQuery = true)
	List<Object> buscarPreguntaPorNumero(@Param("numeroPregunta") Integer numeroPregunta);

	@Query(value = "SELECT pr.pre_numero, pr.pre_descripcion, ors.rsp_valor "
			+ "from preguntas pr JOIN opciones_respuestas ors ON pr.pre_id = ors.pre_id "
			+ "JOIN encuestas_respuestas er ON ors.rsp_id = er.rsp_id", nativeQuery = true)
	List<Object> buscarPreguntas();
}
