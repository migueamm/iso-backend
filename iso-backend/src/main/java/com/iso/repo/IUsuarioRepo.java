package com.iso.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iso.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer> {

}
