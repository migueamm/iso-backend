package com.iso.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.iso.model.OpcionRespuesta;

public interface IOpcionRespuestaRepo extends JpaRepository<OpcionRespuesta, Integer> {

	@Query("from OpcionRespuesta oprsp where oprsp.pregunta.preNumero =:numeroPregunta")
	List<OpcionRespuesta> buscarOpciones(@Param("numeroPregunta") Integer numeroPregunta);

}
