package com.iso.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.iso.model.EncuestaRespuesta;

public interface IEncuestaRespuestaRepo extends JpaRepository<EncuestaRespuesta, Integer> {

}
