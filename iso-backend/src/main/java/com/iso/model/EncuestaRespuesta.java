package com.iso.model;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "encuestas_respuestas")
public class EncuestaRespuesta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer encrspId;

	@ManyToOne
	@JoinColumn(name = "enc_id", nullable = false, foreignKey = @ForeignKey(name = "fk_enc_id"))
	private Encuesta encuesta;

	@ManyToOne
	@JoinColumn(name = "rsp_id", nullable = false, foreignKey = @ForeignKey(name = "fk_rsp_id"))
	private OpcionRespuesta opcionRespuesta;

	public Integer getEncrspId() {
		return encrspId;
	}

	public void setEncrspId(Integer encrspId) {
		this.encrspId = encrspId;
	}

	public Encuesta getEncuesta() {
		return encuesta;
	}

	public void setEncuesta(Encuesta encuesta) {
		this.encuesta = encuesta;
	}

	public OpcionRespuesta getOpcionRespuesta() {
		return opcionRespuesta;
	}

	public void setOpcionRespuesta(OpcionRespuesta opcionRespuesta) {
		this.opcionRespuesta = opcionRespuesta;
	}

}
