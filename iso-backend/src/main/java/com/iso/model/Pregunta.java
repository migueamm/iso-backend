package com.iso.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Información de preguntas")
@Entity
@Table(name = "preguntas")
public class Pregunta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer preId;

	@ApiModelProperty(notes = "Número de la pregunta")
	@Column(name = "pre_numero", nullable = false)
	private Integer preNumero;

	@ApiModelProperty(notes = "La descripción debe tener entre 3 y 500 caracteres")
	@Size(min = 3, max = 500, message = "La descripción debe tener entre 3 y 500 caracteres")
	@Column(name = "pre_descripcion", nullable = false, length = 500)
	private String preDescripcion;

	public Integer getPreId() {
		return preId;
	}

	public void setPreId(Integer preId) {
		this.preId = preId;
	}

	public Integer getPreNumero() {
		return preNumero;
	}

	public void setPreNumero(Integer preNumero) {
		this.preNumero = preNumero;
	}

	public String getPreDescripcion() {
		return preDescripcion;
	}

	public void setPreDescripcion(String preDescripcion) {
		this.preDescripcion = preDescripcion;
	}
}
