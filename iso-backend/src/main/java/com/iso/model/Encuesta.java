package com.iso.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

@Entity
@Table(name = "encuestas")
public class Encuesta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer encId;

	@Column(name = "enc_descripcion", length = 100)
	private String encDescripcion;

	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime encFecha;

	@Column(name = "enc_estado", length = 50)
	private String encEstado;

	@JsonIgnore
	@OneToOne
	@JoinColumn(name = "usu_id", nullable = false, foreignKey = @ForeignKey(name = "fk_usu_id"))
	private Usuario usuario;

	public Integer getEncId() {
		return encId;
	}

	public void setEncId(Integer encId) {
		this.encId = encId;
	}

	public String getEncDescripcion() {
		return encDescripcion;
	}

	public void setEncDescripcion(String encDescripcion) {
		this.encDescripcion = encDescripcion;
	}

	public LocalDateTime getEncFecha() {
		return encFecha;
	}

	public void setEncFecha(LocalDateTime encFecha) {
		this.encFecha = encFecha;
	}

	public String getEncEstado() {
		return encEstado;
	}

	public void setEncEstado(String encEstado) {
		this.encEstado = encEstado;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
