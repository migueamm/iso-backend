package com.iso.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Información de las respuestas")
@Entity
@Table(name = "opciones_respuestas")
public class OpcionRespuesta {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer rspId;

	@ApiModelProperty(notes = "Respuesta")
	@Column(name = "rsp_valor", nullable = false)
	private Integer rspValor;

	@ManyToOne
	@JoinColumn(name = "pre_id", nullable = false, foreignKey = @ForeignKey(name = "fk_pre_id"))
	private Pregunta pregunta;

	public Integer getRspId() {
		return rspId;
	}

	public void setRspId(Integer rspId) {
		this.rspId = rspId;
	}

	public Integer getRspValor() {
		return rspValor;
	}

	public void setRspValor(Integer rspValor) {
		this.rspValor = rspValor;
	}

	public Pregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}
}
