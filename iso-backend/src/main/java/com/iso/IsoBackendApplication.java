package com.iso;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IsoBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(IsoBackendApplication.class, args);
	}

}
