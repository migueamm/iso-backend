package com.iso.service;

import java.util.List;

import com.iso.model.OpcionRespuesta;

public interface IOpcionRespuestaService extends ICRUD<OpcionRespuesta> {

	List<OpcionRespuesta> buscarOpciones(Integer number);

}
