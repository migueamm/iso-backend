package com.iso.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iso.model.Universidad;
import com.iso.repo.IUniversidadRepo;
import com.iso.service.IUniversidadService;

@Service
public class UniversidadServiceImpl implements IUniversidadService {

	@Autowired
	private IUniversidadRepo repo;

	@Override
	public Universidad guardar(Universidad t) {
		return repo.save(t);
	}

	@Override
	public Universidad modificar(Universidad t) {
		return repo.save(t);
	}

	@Override
	public Universidad leerPorId(Integer codigo) {
		Optional<Universidad> optional = repo.findById(codigo);
		return optional.isPresent() ? optional.get() : new Universidad();
	}

	@Override
	public List<Universidad> listarTodo() {
		return repo.findAll();
	}

	@Override
	public void eliminar(Integer codigo) {
		repo.deleteById(codigo);
	}

}
