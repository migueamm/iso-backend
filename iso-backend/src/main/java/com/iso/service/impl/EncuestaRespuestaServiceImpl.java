package com.iso.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iso.model.EncuestaRespuesta;
import com.iso.repo.IEncuestaRespuestaRepo;
import com.iso.service.IEncuestaRespuestaService;

@Service
public class EncuestaRespuestaServiceImpl implements IEncuestaRespuestaService {

	@Autowired
	private IEncuestaRespuestaRepo repo;

	@Override
	public EncuestaRespuesta guardar(EncuestaRespuesta t) {
		return repo.save(t);
	}

	@Override
	public EncuestaRespuesta modificar(EncuestaRespuesta t) {
		return repo.save(t);
	}

	@Override
	public EncuestaRespuesta leerPorId(Integer codigo) {
		Optional<EncuestaRespuesta> optional = repo.findById(codigo);
		return optional.isPresent() ? optional.get() : new EncuestaRespuesta();
	}

	@Override
	public List<EncuestaRespuesta> listarTodo() {
		return repo.findAll();
	}

	@Override
	public void eliminar(Integer codigo) {
		repo.deleteById(codigo);
	}

}
