package com.iso.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iso.model.Encuesta;
import com.iso.repo.IEncuestaRepo;
import com.iso.service.IEncuestaService;

@Service
public class EncuestaServiceImpl implements IEncuestaService {

	@Autowired
	private IEncuestaRepo repo;

	@Override
	public Encuesta guardar(Encuesta t) {
		return repo.save(t);
	}

	@Override
	public Encuesta modificar(Encuesta t) {
		return repo.save(t);
	}

	@Override
	public Encuesta leerPorId(Integer codigo) {
		Optional<Encuesta> optional = repo.findById(codigo);
		return optional.isPresent() ? optional.get() : new Encuesta();
	}

	@Override
	public List<Encuesta> listarTodo() {
		return repo.findAll();
	}

	@Override
	public void eliminar(Integer codigo) {
		repo.deleteById(codigo);
	}

}
