package com.iso.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iso.model.Encuesta;
import com.iso.model.Usuario;
import com.iso.repo.IUsuarioRepo;
import com.iso.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

	@Autowired
	private IUsuarioRepo repo;

	@Override
	public Usuario guardar(Usuario t) {
		Encuesta encuesta = t.getEncuesta();
		encuesta.setUsuario(t);
		return repo.save(t);
	}

	@Override
	public Usuario modificar(Usuario t) {
		return repo.save(t);
	}

	@Override
	public Usuario leerPorId(Integer codigo) {
		Optional<Usuario> optional = repo.findById(codigo);
		return optional.isPresent() ? optional.get() : new Usuario();
	}

	@Override
	public List<Usuario> listarTodo() {
		return repo.findAll();
	}

	@Override
	public void eliminar(Integer codigo) {
		repo.deleteById(codigo);
	}

}
