package com.iso.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iso.model.Pregunta;
import com.iso.repo.IPreguntaRepo;
import com.iso.service.IPreguntaService;

@Service
public class PreguntaServiceImpl implements IPreguntaService {

	@Autowired
	private IPreguntaRepo repo;

	@Override
	public Pregunta guardar(Pregunta t) {
		return repo.save(t);
	}

	@Override
	public Pregunta modificar(Pregunta t) {
		return repo.save(t);
	}

	@Override
	public Pregunta leerPorId(Integer codigo) {
		Optional<Pregunta> optional = repo.findById(codigo);
		return optional.isPresent() ? optional.get() : new Pregunta();
	}

	@Override
	public List<Pregunta> listarTodo() {
		return repo.findAll();
	}

	@Override
	public void eliminar(Integer codigo) {
		repo.deleteById(codigo);
	}

	@Override
	public List<Object> buscarPreguntaPorNumero(Integer numeroPregunta) {
		return repo.buscarPreguntaPorNumero(numeroPregunta);
	}

	@Override
	public List<Object> buscarPreguntas() {
		return repo.buscarPreguntas();
	}

}
