package com.iso.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.iso.model.OpcionRespuesta;
import com.iso.repo.IOpcionRespuestaRepo;
import com.iso.service.IOpcionRespuestaService;

@Service
public class OpcionRespuestaServiceImpl implements IOpcionRespuestaService {

	@Autowired
	private IOpcionRespuestaRepo repo;

	@Override
	public OpcionRespuesta guardar(OpcionRespuesta t) {
		return repo.save(t);
	}

	@Override
	public OpcionRespuesta modificar(OpcionRespuesta t) {
		return repo.save(t);
	}

	@Override
	public OpcionRespuesta leerPorId(Integer codigo) {
		Optional<OpcionRespuesta> optional = repo.findById(codigo);
		return optional.isPresent() ? optional.get() : new OpcionRespuesta();
	}

	@Override
	public List<OpcionRespuesta> listarTodo() {
		return repo.findAll();
	}

	@Override
	public void eliminar(Integer codigo) {
		repo.deleteById(codigo);
	}

	@Override
	public List<OpcionRespuesta> buscarOpciones(Integer number) {
		return repo.buscarOpciones(number);
	}

}
