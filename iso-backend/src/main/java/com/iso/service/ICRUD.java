package com.iso.service;

import java.util.List;

public interface ICRUD<T> {

	T guardar(T t);

	T modificar(T t);

	T leerPorId(Integer codigo);

	List<T> listarTodo();

	void eliminar(Integer codigo);

}
