package com.iso.service;

import java.util.List;

import com.iso.model.Pregunta;

public interface IPreguntaService extends ICRUD<Pregunta> {

	List<Object> buscarPreguntaPorNumero(Integer numeroPregunta);
	
	List<Object> buscarPreguntas();

}
