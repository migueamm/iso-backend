package com.iso.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.iso.exception.ModeloNotFoundException;
import com.iso.model.EncuestaRespuesta;
import com.iso.service.IEncuestaRespuestaService;

@RestController
@RequestMapping("/respuestas")
public class EncuestaRespuestaController {

	@Autowired
	private IEncuestaRespuestaService service;

	@GetMapping
	public ResponseEntity<List<EncuestaRespuesta>> listar() {
		List<EncuestaRespuesta> respuestas = service.listarTodo();
		return new ResponseEntity<List<EncuestaRespuesta>>(respuestas, HttpStatus.OK);
	}

	@GetMapping("/{codigo}")
	public ResponseEntity<EncuestaRespuesta> listarPorId(@PathVariable("codigo") Integer codigo) {
		EncuestaRespuesta respuesta = service.leerPorId(codigo);
		if (respuesta == null) {
			throw new ModeloNotFoundException("Código no encontrado " + codigo);
		}
		return new ResponseEntity<EncuestaRespuesta>(respuesta, HttpStatus.OK);
	}

	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<Object> guardar(@Valid @RequestBody EncuestaRespuesta respuesta) {
		EncuestaRespuesta resp = service.guardar(respuesta);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{/codigo}")
				.buildAndExpand(resp.getEncrspId()).toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<Object> modificar(@Valid @RequestBody EncuestaRespuesta respuesta) {
		service.modificar(respuesta);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping("/{codigo}")
	private ResponseEntity<Object> eliminar(@PathVariable("codigo") Integer codigo) {
		EncuestaRespuesta respuesta = service.leerPorId(codigo);
		if (respuesta != null) {
			throw new ModeloNotFoundException("Código no encontrado para eliminar " + codigo);
		} else {
			service.eliminar(codigo);
		}
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
