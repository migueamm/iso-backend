package com.iso.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.iso.exception.ModeloNotFoundException;
import com.iso.model.Universidad;
import com.iso.service.IUniversidadService;

@RestController
@RequestMapping("/universidades")
public class UniversidadController {

	@Autowired
	private IUniversidadService service;

	@GetMapping
	public ResponseEntity<List<Universidad>> listar() {
		List<Universidad> universidades = service.listarTodo();
		return new ResponseEntity<List<Universidad>>(universidades, HttpStatus.OK);
	}

	@GetMapping("/{codigo}")
	public ResponseEntity<Universidad> listarPorId(@PathVariable("codigo") Integer codigo) {
		Universidad universidad = service.leerPorId(codigo);
		if (universidad == null) {
			throw new ModeloNotFoundException("Código no encontrado " + codigo);
		}
		return new ResponseEntity<Universidad>(universidad, HttpStatus.OK);
	}

	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<Object> guardar(@Valid @RequestBody Universidad universidad) {
		Universidad uni = service.guardar(universidad);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{/codigo}").buildAndExpand(uni.getUniId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<Object> modificar(@Valid @RequestBody Universidad universidad) {
		service.modificar(universidad);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping("/{codigo}")
	public ResponseEntity<Object> eliminar(@PathVariable("codigo") Integer codigo) {
		Universidad universidad = service.leerPorId(codigo);
		if (universidad == null) {
			throw new ModeloNotFoundException("Código no encontrado para eliminar " + codigo);
		} else {
			service.eliminar(codigo);
		}
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
