package com.iso.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.iso.exception.ModeloNotFoundException;
import com.iso.model.Usuario;
import com.iso.service.IUsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	private IUsuarioService service;

	@GetMapping
	public ResponseEntity<List<Usuario>> listar() {
		List<Usuario> usuarios = service.listarTodo();
		return new ResponseEntity<List<Usuario>>(usuarios, HttpStatus.OK);
	}

	@GetMapping("/{codigo}")
	public ResponseEntity<Usuario> listarPorCodigo(@PathVariable("codigo") Integer codigo) {
		Usuario usuario = service.leerPorId(codigo);
		if (usuario == null) {
			throw new ModeloNotFoundException("Código no encontrado: " + codigo);
		}
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}

	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<Usuario> guardar(@Valid @RequestBody Usuario usuario) {
		Usuario u = service.guardar(usuario);
		return new ResponseEntity<Usuario>(u, HttpStatus.CREATED);
	}

	@PutMapping
	public ResponseEntity<Object> modificar(@Valid @RequestBody Usuario usuario) {
		service.modificar(usuario);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping("/{codigo}")
	public ResponseEntity<Object> eliminar(@PathVariable("codigo") Integer codigo) {
		Usuario usuario = service.leerPorId(codigo);
		if (usuario == null) {
			throw new ModeloNotFoundException("Código no encontrado " + codigo);
		} else {
			service.eliminar(codigo);
		}
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
