package com.iso.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.iso.exception.ModeloNotFoundException;
import com.iso.model.Encuesta;
import com.iso.service.IEncuestaService;

@RestController
@RequestMapping("/encuestas")
public class EncuestaController {

	@Autowired
	private IEncuestaService service;

	@GetMapping
	public ResponseEntity<List<Encuesta>> listar() {
		List<Encuesta> encuestas = service.listarTodo();
		return new ResponseEntity<List<Encuesta>>(encuestas, HttpStatus.OK);
	}

	@GetMapping("/{codigo}")
	public ResponseEntity<Encuesta> listarPorId(@PathVariable("codigo") Integer codigo) {
		Encuesta encuesta = service.leerPorId(codigo);
		if (encuesta == null) {
			throw new ModeloNotFoundException("Código no encontrado " + codigo);
		}
		return new ResponseEntity<Encuesta>(encuesta, HttpStatus.OK);
	}

	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<Object> guardar(@Valid @RequestBody Encuesta encuesta) {
		Encuesta enc = service.guardar(encuesta);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{/codigo}").buildAndExpand(enc.getEncId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<Object> modificar(@Valid @RequestBody Encuesta encuesta) {
		service.modificar(encuesta);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping("/{codigo}")
	public ResponseEntity<Object> eliminar(@PathVariable("codigo") Integer codigo) {
		Encuesta encuesta = service.leerPorId(codigo);
		if (encuesta == null) {
			throw new ModeloNotFoundException("Código no encontrado para eliminar " + codigo);
		} else {
			service.eliminar(codigo);
		}
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
