package com.iso.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.iso.exception.ModeloNotFoundException;
import com.iso.model.Pregunta;
import com.iso.service.IPreguntaService;

@RestController
@RequestMapping("/preguntas")
public class PreguntaController {

	@Autowired
	private IPreguntaService service;

	@GetMapping
	public ResponseEntity<List<Pregunta>> listar() {
		List<Pregunta> preguntas = service.listarTodo();
		return new ResponseEntity<List<Pregunta>>(preguntas, HttpStatus.OK);
	}

	@GetMapping("/{codigo}")
	public ResponseEntity<Pregunta> listarPorCodigo(@PathVariable("codigo") Integer codigo) {
		Pregunta pregunta = service.leerPorId(codigo);
		if (pregunta == null) {
			throw new ModeloNotFoundException("Código no encontrado " + codigo);
		}
		return new ResponseEntity<Pregunta>(pregunta, HttpStatus.OK);
	}

	@GetMapping("/pregunta/{numeroPregunta}")
	public ResponseEntity<List<Object>> buscarPreguntaPorNumero(@PathVariable("numeroPregunta") Integer numero) {
		List<Object> preguntas = service.buscarPreguntaPorNumero(numero);
		return new ResponseEntity<List<Object>>(preguntas, HttpStatus.OK);
	}

	@GetMapping("/todas")
	public ResponseEntity<List<Object>> buscarPreguntas() {
		List<Object> preguntas = service.buscarPreguntas();
		return new ResponseEntity<List<Object>>(preguntas, HttpStatus.OK);
	}

	@PostMapping(produces = "application/json", consumes = "application/json")
	public ResponseEntity<Object> guardar(@Valid @RequestBody Pregunta pregunta) {
		Pregunta p = service.guardar(pregunta);
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("{/codigo}").buildAndExpand(p.getPreId())
				.toUri();
		return ResponseEntity.created(location).build();
	}

	@PutMapping
	public ResponseEntity<Object> modificar(@Valid @RequestBody Pregunta pregunta) {
		service.modificar(pregunta);
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

	@DeleteMapping("/{codigo}")
	public ResponseEntity<Object> eliminar(@PathVariable("codigo") Integer codigo) {
		Pregunta pregunta = service.leerPorId(codigo);
		if (pregunta == null) {
			throw new ModeloNotFoundException("Código no existe para eliminar " + codigo);
		} else {
			service.eliminar(codigo);
		}
		return new ResponseEntity<Object>(HttpStatus.OK);
	}

}
